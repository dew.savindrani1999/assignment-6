#include <stdio.h>

void Row(int y);
void sequence(int NumberOfRows);
int NumberOfRows=1;
int y;

void Row(int y)
{

    if(y>0)
    {
        sequence(NumberOfRows);
        printf("\n");
        NumberOfRows++;
        Row(y-1);

    }
}


void sequence(int NumberOfRows)
{
    if(NumberOfRows>0)
    {
        printf("%d",NumberOfRows);
        sequence(NumberOfRows-1);
    }
}

int main()
{

    printf("Enter a number: \n");
    scanf("%d",&y);
    Row(y);
    return 0;
}
